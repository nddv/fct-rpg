#!/bin/python3
#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re
import argparse
from os import path
from subprocess import run
from pathlib import Path
from typing import Union
from shutil import which

import map_conversion


PRESET_IDS = {"linux/x11-client": 0,
              "linux/x11-server": 1,
              "windows-client"  : 2}

_PROJECT_NAME = "FCTRPG"

_EXPORT_PRESETS_FILE = "export_presets.cfg"


def _generate_export_files_string(files: list) -> str:
    result = ""
    for filename in files:
        result += " \"" + filename + "\","

    return result[1:-1]


# Actually didn't end up using these functions. Might want to remove them.
"""
# Note that in addition these synonyms should be case insensitive.
def _get_export_name_synonyms(build: str) -> list:
    client_server = "server" if "server" in build else "client"
    linux_options = ["linux/x11-", "linux-x11-", "linux/x11/", "linuxx11",
                     "x11-", "linux-", "x11/", "linux/"]

    if "linux/x11" in build:
        return list(map(lambda x: x + client_server, linux_options))
    else:
        return [] # ToDo: Expand for other platforms.


def _get_export_name(name: str) -> str:
    for build_name in PRESET_IDS:
        if name.lower() in _get_export_name_synonyms(build_name):
            return build_name
    raise ValueError("Invalid export name.")
"""


def _is_server(export_name: Union[str, list[str]]) -> bool:
    if isinstance(export_name, list):
        for build in export_name:
            if "server" in build:
                return True
        return False
    else:  # str
        return "server" in export_name

def _get_build_path(export_name: str) -> str:
    if _is_server(export_name):
        return "builds/server/" + _PROJECT_NAME + "-server.pck"
    elif "linux" in export_name:
        return "builds/linux/"
    elif "win" in export_name:
        return "builds/win/"
    elif "mac" in export_name:
        return "builds/mac/"
    else:
        return ""


def export_all(godot: str, proj_root: str, debug: bool = False,
               prep_export: bool = True, allow_map_conversion: bool = True,
               godot_server: Union[str, None] = None):
    exports(godot, proj_root, list(PRESET_IDS.keys()), debug, prep_export,
            allow_map_conversion, godot_server)


# Bad name?
# Too many arguments. Maybe group them into an object?
def exports(godot: str, proj_root: str, presets: list[str],
            debug: bool = True, prep_export: bool = True,
            allow_map_conversion: bool = True,
            godot_server: Union[str, None] = None):
    if allow_map_conversion and _is_server(presets):
        map_conversion.convert_maps(proj_root)

    # We do not want to convert maps more than once.
    for preset in presets:
        export(godot, proj_root, preset, debug, prep_export, False, godot_server)


def export(godot: str, proj_root: str, preset: str, debug: bool = True,
           prep_export: bool = True, allow_map_conversion: bool = True,
           godot_server: Union[str, None] = None):
    if prep_export:
        prepare_export(proj_root, preset, allow_map_conversion)
    godot_path = path.expanduser(path.expandvars(godot))
    run([godot_path, "--export-debug" if debug else "--export",
        preset], cwd=path.abspath(proj_root))
    if _is_server(preset) and godot_server != None:
        run(["cp", str(which(godot_server)), _get_build_path(preset)[:-4] + ".x86_64"], cwd=path.abspath(proj_root))
        


def prepare_export_all(proj_root: str, allow_convert_maps: bool = True):
    prepare_exports(proj_root, list(PRESET_IDS.keys()), allow_convert_maps)


def prepare_exports(proj_root: str, names: list[str],
                    allow_convert_maps: bool = True):
    for name in names:
        prepare_export(proj_root, name, allow_convert_maps)


def prepare_export(proj_root: str, name: str, allow_convert_maps: bool = True):
    if allow_convert_maps and _is_server(name):
        map_conversion.convert_maps(proj_root)
    client_server = "server" if "server" in name else "client"
    update_export_presets(proj_root, PRESET_IDS[name], client_server)


def update_export_presets(proj_root: str, preset_id: int,
                          build_specific_dir: str):
    p = Path(proj_root)
    file_paths = []
    for element in p.glob("**/*"):  # rglob?
        if element.is_file() and re.match(".*?((default_env.tres)|(dialogue.cfg)|(default_bus_layout.tres)|(src\\/Entry\\.(tscn|gd))|(addons\\/)|(src\\/{}\\/)|(src\\/common\\/)|(locale\\/.+?\\.po$))".format(build_specific_dir), str(element), flags=re.MULTILINE):
            file_paths.append(str(element))

    # Project root is res://.
    for index, file in enumerate(file_paths):
        file_paths[index] = "res://" + path.relpath(file, proj_root)

    # String with the updated list of files.
    files_string = _generate_export_files_string(file_paths)

    # Replace old file list with updated list
    with open(proj_root + "/" + _EXPORT_PRESETS_FILE, "r+",
              encoding="utf8") as presets_file:
        sect_re = "\\[preset\\." + str(preset_id) + "\\].*?\\[preset\\." \
                  + str(preset_id) + "\\.options\\]"
        contents = presets_file.read()

        section = re.findall(sect_re, contents, re.DOTALL)[0]
        section = re.sub("(?<=export_files=PoolStringArray\\( ).*?(?= \\))",
                         files_string, section)
        contents = re.sub(sect_re, section, contents, flags=re.DOTALL)

        presets_file.seek(0)
        presets_file.truncate()
        presets_file.write(contents)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="FCT-RPG exporter script",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    subparsers = parser.add_subparsers(required=True, dest="action")

    # Arguments are repeated across subparsers, because mixing base-parser
    # arguments with subparsers gets weird.
    export_parser = subparsers.add_parser("export",
                                          description="export to a target",
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    export_parser.add_argument("-t", "--target", help="select export targets; give \"all\" to select all targets",
                               choices=["all"] + list(PRESET_IDS.keys()),
                               nargs="+", required=True)
    export_parser.add_argument("-r", "--root",
                               help="project root (res://)", default="..")
    export_parser.add_argument("-g", "--godot", help="any godot binary with export capabilities",
                               default="godot")
    export_parser.add_argument("-s", "--godot-server", help="binary to run the server with")
    export_parser.add_argument("-d", "--debug", help="export with debug features",
                               action="store_true", default=False)
    export_parser.add_argument("-p", "--disable-prepare-export",
                               help="don't convert maps, even if a target is server, nor update export_presets.cfg",
                               action="store_true", default=False)
    export_parser.add_argument("-c", "--disable-map-conversion",
                               help="do not convert maps, even if a target is server; no effect if prepare-export already disabled",
                               action="store_true", default=False)

    prep_parser = subparsers.add_parser("prepare",
                                        description="update export_presets.cfg and optionally convert maps without exporting",
                                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # ToDo: Maybe set default to own platform?
    prep_parser.add_argument("-t", "--target",
                             help="select export targets; give \"all\" to select all targets",
                             choices=["all"] + list(PRESET_IDS.keys()),
                             nargs="+", required=True)
    prep_parser.add_argument("-r", "--root", help="project root (res://)",
                             default="..")
    prep_parser.add_argument("-c", "--disable-map-conversion",
                             help="do not convert maps, even if a target is server",
                             action="store_true", default=False)

    args = parser.parse_args()

    if args.action == "prepare":
        if "all" in args.target:
            prepare_export_all(args.root, not args.disable_map_conversion)
        else:
            prepare_exports(args.root, args.target,
                            not args.disable_map_conversion)
    else:  # export
        if "all" in args.target:
            export_all(args.godot, args.root, args.debug,
                       not args.disable_prepare_export,
                       not args.disable_map_conversion,
                       args.godot_server)
        else:
            exports(args.godot, args.root, args.target, args.debug,
                    not args.disable_prepare_export,
                    not args.disable_map_conversion, args.godot_server)
    
