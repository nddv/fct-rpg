#!/bin/python3
#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import re

def convert_maps(proj_root: str):
    map_dir = proj_root + "/src/client/maps/"
    out_dir = proj_root + "/src/server/maps/"

    for dir_element in os.listdir(map_dir):
        if ".tscn" in dir_element:
            tm_dir = map_dir + dir_element
            out_path = out_dir + dir_element
            with open(tm_dir, "r", encoding="utf8") as tm_file, open(out_path, "w", encoding="utf8") as out_file:
                tm_content = tm_file.read()

                # Replace all textures with a placeholder texture.
                tm_content = re.sub("res:.*?\\.(png|jpeg|jpg|tiff|bmp|webp|tga)(?=\\\" type=\\\"Texture\\\")", "res://src/server/placeholder.png", tm_content)
                tm_content = re.sub("res:.*?\\.(ogg|wav|webm|mp3|opus|avi|3gpp|flac|3gp|oga|mogg|m4a)(?=\\\" type=\\\"AudioStream\\\")", "res://src/server/placeholder.wav", tm_content)

                # All client specific scenes should have their paths changed to be that of the corresponding server scene.
                tm_content = tm_content.replace("res://src/client/", "res://src/server/") # Any reference to the opposing build should fail anyway. We don't need to worry about being too overzealous here.

                # Remove client specific node properties
                tm_sects = re.findall("\\[node name.*?(?=\\[node name)|\\[node name.*?$", tm_content, flags=re.DOTALL)
                for sect in tm_sects:
                    if "type=\"TileMap\"" in sect:
                        continue
                    new_sect = re.sub("^client_.*?$", "", sect, flags=re.MULTILINE)
                    tm_content = tm_content.replace(sect, new_sect, 1)

                # Some resources may not have a server equivalent. We must handle those cases by removing them and any references to them.
                extres_paths = re.findall("(?<=\\[ext_resource path=\\\").+?(?=\\\" type=)", tm_content)
                sub_resources = re.findall("\\[sub_resource.*?(?=\\[sub_resource)|\\[sub_resource.*?(?=\\[node)", tm_content, re.DOTALL)
                for res_path in extres_paths:
                    path = proj_root + "/" + res_path.replace("res://", "")
                    if not os.path.exists(path):
                        ext_res_line = re.search("\\[ext_resource path=\\\"" + res_path + "\\\".*", tm_content).group(0)
                        id = int(re.search("(?<=id=)\\d+", ext_res_line).group(0))
                        tm_content = tm_content.replace(ext_res_line, "", 1)

                        # We must remove subresources that reference the invalid resources, to avoid errors.
                        for sub_resource in sub_resources:
                            if "ExtResource( " + str(id) + " )" in sub_resource:
                                tm_content = tm_content.replace(sub_resource, "")
                    
                out_file.write(tm_content)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        convert_maps(sys.argv[1])
    elif len(sys.argv) == 1 and os.path.exists(os.getcwd() + "/project.godot"):
        convert_maps(os.getcwd())
    elif len(sys.argv) == 1 and os.path.exists(os.getcwd() +
                                               "/../project.godot"):
        convert_maps(os.getcwd() + "/../")
    else:
        print("Usage: ./map_conversion.py [project_root]")
        sys.exit(1)
