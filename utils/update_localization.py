#!/bin/python3
#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

LOCALES = ["en_US", "pt_PT", "de_DE"]
DIALOGUE_PATH = "src/client/dialogue"

import sys
from pathlib import Path
from subprocess import run

def generate_pot(proj_root = ".."):
    run(["pybabel", "extract", "-F", "locale/babelrc", "-k", "text", "-k", "LineEdit/placeholder_text", "-k", "tr", "-o", "locale/messages.pot", "src"], cwd=proj_root)
    dialogue_path = Path(proj_root + "/" + DIALOGUE_PATH)
    for f in dialogue_path.glob("*.po"):
        run(["msgfilter", "-i", str(f.relative_to(proj_root)), "-o", "locale/" + f.with_suffix(".pot").with_stem(f.stem.rsplit("-", maxsplit=1)[0]).name, "--keep-header", "true"], cwd=proj_root)
        run("cp " + str(f.relative_to(proj_root)).replace(".po", ".*") + " locale/", cwd=proj_root, shell=True)

def generate_po(proj_root = ".."):
    p = Path(proj_root + "/locale/")
    for pot in p.glob("*.pot"):
        for locale in LOCALES:
            f = pot.with_stem(pot.stem + "-" + locale).with_suffix(".po")
            if f.is_file():
                run(["msgmerge", "--update", "--backup=none", str(f.relative_to(proj_root)), str(pot.relative_to(proj_root))], cwd=proj_root)
            else:
                run(["msginit", "--no-translator", "--input=" + str(pot.relative_to(proj_root)), "--locale=" + locale, "-o", str(f.relative_to(proj_root))], cwd=proj_root)

if __name__ == "__main__":
    if len(sys.argv) == 3:
        if sys.argv[1] == "generate_pot":
            generate_pot(sys.argv[2])
        if sys.argv[1] == "generate_po":
            generate_po(sys.argv[2])
        if sys.argv[1] == "all":
            generate_pot(sys.argv[2])
            generate_po(sys.argv[2])
    elif len(sys.argv) == 2:
        if sys.argv[1] == "generate_pot":
            generate_pot()
        if sys.argv[1] == "generate_po":
            generate_po()
        if sys.argv[1] == "all":
            generate_pot()
            generate_po()
    else:
        print("Usage: ./update_localization.sh {generate_pot|generate_po} [proj_root]")

