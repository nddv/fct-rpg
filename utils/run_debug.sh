#!/bin/bash

#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

INSTANCE_NUM=$1
GODOT=${GODOT:-godot} # ToDo: Use command line arguments with getopts instead, rather than env.
PROJ_PATH=$(dirname $0)/${PROJ_PATH:-".."}
WAIT_TIME=4

#if [ $RUN_SERVER -e 0 ]
if [ "$2" = "run-server" ]
then
    $GODOT --server --port 4242 --max-players 10 --name test_world --admin-user test-user --path $PROJ_PATH --debug-collisions &
    sleep $WAIT_TIME # At the moment not needed, but in the future we may automatically connect clients to the server, in which case we need to wait.
fi

while [ $((INSTANCE_NUM--)) -gt 0 ]
do 
    $GODOT --path $PROJ_PATH --debug-collisions &
done

