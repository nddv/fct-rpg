# FCT RPG

MMORPG based on NOVA FCT.

## What we envisioned
FCTRPG (name to be changed) was decided to be become an RPG based on the reality of NOVA FCT, as seen through the lens of its students. It is supposed to in the future touch the topics that are dearest to our hearts (and some that are less so), such as all the challenges and hardships we have to face throughout our academic life (and CLIP being nowhere to be found, having wandered away again in its old and demented state<!--I feel uncomfortable mentioning dementia here, since it does indeed cause people to wander around, but I feel like I am making fun of people with dementia, whose condition I do not feel comfortable laughing about, due to how tragic the situation is in reality-->). Through discussion, it was decided that FCTRPG should become a game with the following features and characteristics:
* Multiplayer
* Top-down gridbased
* Turn-based tactical combat
* Parties (as in a set of people cooperating)
* Character development
* And more <!-- I very likely forgot stuff, so I put this line here-->

## Getting started
This game only runs on version 3.5.x of Godot and you need [Python3](https://www.python.org/) to run some scripts. Python generally comes preinstalled on Linux, but you'll normally need to install it on Windows. You also need to download the [Godot Dialogue Manager](https://github.com/nathanhoad/godot_dialogue_manager) addon and if you want to run the server, the [Godot SQLite](https://github.com/2shady4u/godot-sqlite) addon, both of which can be easily acquired from the Godot asset library (just search for them and click Download and select the addons directory).
### Running via a script
Before running the server you need to generate the maps for the server, by running the map_conversion.py script in utils:
```
python3 map_conversion.py
```

To test out the game you can use the utils/run_debug.sh script to do a test run of the game. For that, run the following command as an example:
```
./run_debug.sh 2 run-server
```
This launches two client windows and the server with a default config (port 4242).
(godot must be in PATH or have its path stored in the GODOT environment variable for this to work)
Optionally, if you aren't using Linux, you can open two Godot windows and open src/client/Main.tscn and in the other open src/server/Main.tscn and run the scene in each window (see [Running from the editor](#running-from-the-editor)).

### Running from the editor
#### Server
To run the server from the editor, you should run the project (F5) or run the src/server/Main.tscn scene. The project is configure to automatically pass some default runtime arguments (see Display > Editor > Main run args in the Project settings).
#### Client
To run the client from the editor, you must open the src/client/Main.tscn scene and run it (F6). Running the project (F5) will start the server instead.

## Exporting
To export the project you should use the utils/export.py script:
```
./export.py export -t windows-client -r .. -g /path/to/godot
```

Type `./export.py export --help` for more information on exporting.

## Command line arguments
The server accepts commandline options. These must be present when creating a new world. It then saves the values given, so you only need once to give these arguments once. To change any of them supply that argument on the commandline in a later run. The server will update its config for that option and for then on use that value for that world.
### `--server`
Run the server instead of the client. Not relevant when running the exported game, since in that case it can only run either the server or the client.
### `--port`
Port the server should listen on
### `max-players`
Max amount of peers allowed to connect to the server.
### `name`
World name. Creates a folder of that name in user://server
### `admin-user`
User that regardless of having the `privs` privilige or not can grant anyone including themselves all possible priviliges. This option exists to guarantee that there's always at least one person with those priviliges.

## Contributing
Want to help out? Check out [CONTRIBUTING.md](CONTRIBUTING.md)

## Further questions
If you have any further questions contact us on Discord, so we can answer your questions and possibly improve this document.
