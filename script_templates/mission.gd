#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Mission

const mission_name: String = ""
const manual_update: bool = false

func meets_assignment_requirements(player: Player) -> bool:
	return false

func meets_success_requirements(player: Player) -> bool:
	return false

func assignment_effects_common(player: Player) -> void:
	pass

func assignment_effects_server(player: Player) -> void:
	pass

func assignment_effects_client(player: Player) -> void:
	pass

func assignment_effects_permanent(player: Player) -> void:
	pass

func success_effects_common(player: Player) -> void:
	pass

func success_effects_server(player: Player) -> void:
	pass

func success_effects_client(player: Player) -> void:
	pass

func success_effects_permanent(player: Player) -> void:
	pass
