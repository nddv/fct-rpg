#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

func _ready() -> void:
	Events.connect("player_joined", self, "player_joined")
	Events.connect("player_left", self, "player_left")

remote func send_message(message: String):
	var id: int = get_tree().get_rpc_sender_id()

	if id in Network.authenticated_players.keys() and message.length() > 0 and message.length() < Globals.MAX_MESSAGE_LENGTH:
		if message[0] == "/":
			command(id, message.substr(1))
		elif WorldRWS.has_privilige(Network.authenticated_players[id], "shout"):
			rpc("receive_message", Network.authenticated_players[id], message)
		else:
			rpc_id(id, "server_message", Globals.SERVER_RESPONSE.MISSING_PRIVILIGE, {})

func command(id: int, cmd_message: String) -> void:
	var args: Array = cmd_message.split(" ", false)

	match (args[0]):
		"me":
			cmdme(id, cmd_message)
		"grant":
			cmdgrant(id, args)
		"revoke":
			cmdrevoke(id, args)
		"kick":
			cmdkick(id, args)
		_:
			rpc_id(id, "server_message", Globals.SERVER_RESPONSE.UNKNOWN_COMMAND, {})

func cmdme(id: int, message: String) -> void:
	var username: String = Network.authenticated_players[id]
	if WorldRWS.has_privilige(username, "shout"):
		rpc("me", username, message.strip_edges().trim_prefix("me "))
	else:
		rpc_id(id, "server_message", Globals.SERVER_RESPONSE.MISSING_PRIVILIGE, {})

func cmdgrant(id: int, args: Array) -> void:
	var granter: String = Network.authenticated_players[id]
	if WorldRWS.has_privilige(granter, "privs") or granter == Globals.server_options["admin-user"]:
		var grantee: String = args[1]
		var priv: String = args[2]
		WorldRWS.add_privilige(grantee, priv)
		rpc_id(id, "server_message", Globals.SERVER_RESPONSE.PRIV_GRANTED, {"granter": granter, "priv": priv, "grantee": grantee})
		if granter != grantee: # Prevent sending message twice
			rpc_id(Network.get_username_id(grantee), "server_message", Globals.SERVER_RESPONSE.PRIV_GRANTED, {"granter": granter, "priv": priv, "grantee": grantee})
	else:
		rpc_id(id, "server_message", Globals.SERVER_RESPONSE.MISSING_PRIVILIGE, {})

func cmdrevoke(id: int, args: Array) -> void:
	var revoker: String = Network.authenticated_players[id]
	if WorldRWS.has_privilige(revoker, "privs") or revoker == Globals.server_options["admin-user"]:
		var revokee: String = args[1]
		var priv: String = args[2]
		WorldRWS.remove_privilige(revokee, priv)
		rpc_id(id, "server_message", Globals.SERVER_RESPONSE.PRIV_REVOKED, {"revoker": revoker, "priv":priv, "revokee":revokee})
		if revoker != revokee:
			rpc_id(Network.get_username_id(revokee), "server_message", Globals.SERVER_RESPONSE.PRIV_REVOKED, {"revoker": revoker, "priv":priv, "revokee":revokee})
		else:
			rpc_id(id, "server_message", Globals.SERVER_RESPONSE.MISSING_PRIVILIGE, {})

func cmdkick(id: int, args: Array) -> void:
	var kicker: String = Network.authenticated_players[id]

	if WorldRWS.has_privilige(kicker, "ban"):
		var kicked: String = args[1]
		var kicked_id: int = Network.get_username_id(kicked)
		Network.rpc_id(kicked_id, "kick")
		Events.emit_signal("player_left", kicked_id)
		rpc("server_message", Globals.SERVER_RESPONSE.PLAYER_KICKED, {"kicker": kicker, "kicked": kicked})
	else:
		rpc_id(id, "server_message", Globals.SERVER_RESPONSE.MISSING_PRIVILIGE, {})

func player_joined(player_id: int, username: String) -> void:
	rpc("player_joined", username)

func player_left(player_id: int, username: String) -> void:
	rpc("player_left", username)
