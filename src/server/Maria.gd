#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends NPCS

# Number of msecs between moves.
export(int) var movement_rate setget set_movement_rate

onready var timer = $Timer

var rng: RandomNumberGenerator = RandomNumberGenerator.new()

func _ready() -> void:
	set_movement_rate(movement_rate)

func set_movement_rate(new_rate: int):
	movement_rate = new_rate
	if is_inside_tree():
		timer.wait_time = Globals.MOVE_TIME_SECONDS + new_rate / 1000

func get_surrounding_free_tiles():
	var result: Array
	var old_cast_to: Vector2 = raycast.cast_to
	for i in [Vector2.UP, Vector2.LEFT, Vector2.DOWN, Vector2.RIGHT]:
		raycast.cast_to = i * Globals.TILE_SIZE
		raycast.force_raycast_update()
		if not raycast.is_colliding():
			result.append(i)
	raycast.cast_to = old_cast_to
	return result

func _on_Timer_timeout() -> void:
	var possible_choices: Array = get_surrounding_free_tiles()
	if enable_moving and not possible_choices.empty():
		position += possible_choices[rng.randi_range(0, possible_choices.size() - 1)] * Globals.TILE_SIZE
		rpc("change_pos", position)
