#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Player

class_name PlayerS

var username: String

remote func movement_input(input: String) -> void:
#	if update_move_time():
	if !move(input): # ToDo: Check if too fast
		Events.emit_signal("full_sync", get_tree().get_rpc_sender_id())
	else:
		rpc("change_pos", position)

# Attempt to move the player
#
# Returns true if allowed to, false otherwise (for example due to collision).
func move(input_dir: String) -> bool:
	var new_position: Vector2 = position + INPUT_DIRS[input_dir] * Globals.TILE_SIZE
	ray.cast_to = INPUT_DIRS[input_dir] * Globals.TILE_SIZE
	ray.collide_with_areas = true # to allow to collisions with players and other moving objects
	for body in get_overlapping_areas(): # to prevent getting stuck from being spawned inside something
		ray.add_exception(body)
	ray.force_raycast_update()
	if !ray.is_colliding():
		position = new_position
		ray.clear_exceptions()
		ray.add_exception(self)
		return true
	else:
		ray.clear_exceptions()
		ray.add_exception(self)
		return false

func is_movement_action(event: String) -> bool:
	for input in INPUT_DIRS:
		if input == event:
			return true
	return false
