#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

func _ready():
	Events.connect("player_joined", self, "_on_player_joined")
	Events.connect("player_left", self, "_on_player_left")
	Events.connect("full_sync", self, "full_sync")

	var options: Dictionary = get_conf()
	Globals.server_options = options

	var port: int = int(options["port"])
	var max_players: int = int(options["max-players"])
	var game_name: String = options["name"]
	var world_path: String = "user://server/worlds/" + game_name
	if WorldRWS.open_world(world_path, game_name) or "edit" in options.keys():
		WorldRWS.update_world_config(world_path, options)
	if Network.server_set_up(port, max_players) == OK:
		print("Created server.")

	var map = preload("res://src/server/maps/test-tilemap.tscn").instance()
	map.name = "m" + str(0)
	add_child(map)

func get_conf() -> Dictionary:
	var options: Dictionary = get_arg_opts()
	var conf: ConfigFile = ConfigFile.new()
	var game_name: String = options["name"]
	if conf.load("user://server/worlds/" + game_name + "/world.cfg") != ERR_FILE_NOT_FOUND:
		var params: Array = ["port", "max-players"]
		for param in params:
			if !(param in options.keys()):
				options[param] = conf.get_value("general", param)
	return options

func get_arg_opts() -> Dictionary:
	var options: Dictionary = {}
	var args: Array = OS.get_cmdline_args()
	for i in len(args):
		if args[i].begins_with("--"):
			if i + 1 < len(args) and not args[i + 1].begins_with("--"):
				options[args[i].trim_prefix("--")] = int(args[i + 1]) if args[i+1].is_valid_integer() else args[i+1]
			else:
				options[args[i].trim_prefix("--")] = ""
	return options

func _on_player_joined(id: int, username: String) -> void:
	var new_player = preload("res://src/server/Player.tscn").instance()
	var player_pos = WorldRWS.get_player_pos(username)
	new_player.name = "p" + str(id)
	new_player.username = username
	new_player.set_network_master(id)
	if player_pos == null:
		new_player.position = Vector2(7.5, 5.5) * Globals.TILE_SIZE
		$"m0".add_child(new_player)
	else:
		new_player.position = (player_pos[1] + Vector2(0.5, 0.5)) * Globals.TILE_SIZE
		get_node("m" + str(player_pos[0])).add_child(new_player)

	full_sync(id)
	rpc("add_player", 0, id, username, new_player.position)

func full_sync(id: int) -> void:
	print("Full sync on peer ", id, "!")
	rpc_id(id, "full_sync", generate_game_state())

func _on_player_left(id: int, username: String) -> void:
	Network.authenticated_players.erase(id)
	for player in get_tree().get_nodes_in_group("player"):
		if player.name == "p" + str(id):
			WorldRWS.save_player_pos(player.username, 0, player.get_cell_coordinates())
			player.get_parent().remove_child(player)
			player.queue_free()
			rpc("remove_player", id)
			return

func generate_game_state() -> Array:
	var game_state: Array = Array()
	game_state.append({})
	for map in len(game_state):
		game_state[map]["player"] = {}
		game_state[map]["npc"] = {}
	for child in $"m0".get_children():
		if child.is_in_group("player"):
			var story: Node = child.get_node("Story")
			var active_missions: Array
			var completed_missions: Array
			for mis in story.active_missions:
				active_missions.append(mis.mission_name)
			for mis in story.completed_missions:
				completed_missions.append(mis.mission_name)
			game_state[0]["player"][child.get_network_master()] = {"username": child.username, "position": child.position, "active_missions": active_missions, "completed_missions": completed_missions}
		elif child.is_in_group("npc"):
			game_state[0]["npc"][child.name] = {"position": child.position}
	return game_state
