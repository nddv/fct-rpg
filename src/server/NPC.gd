#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends NPC

class_name NPCS

onready var raycast: RayCast2D = $RayCast2D

var actioners: Array = []
var enable_moving: bool = true

remote func actioned() -> void:
	var id: int = get_tree().get_rpc_sender_id()
	if not (id in actioners):
		actioners.append(id) # ToDo: Continue
		enable_moving = false

remote func unactioned() -> void:
	var id: int = get_tree().get_rpc_sender_id()
	actioners.erase(id)
	if actioners.empty():
		enable_moving = true
