#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node


func _ready() -> void:
	# Run server or client, based on export.
	var scene_path : String = "res://src/client/Main.tscn"

	var args: Array = OS.get_cmdline_args()
	if OS.has_feature("standalone") and OS.has_feature("dedicated_server"):
		scene_path = "res://src/server/Main.tscn"
	elif !OS.has_feature("standalone") and "--server" in args:
		scene_path = "res://src/server/Main.tscn"

	if get_tree().change_scene(scene_path) != OK:
		print("Can't load correct main scene. Probably a problem with the export.")
		get_tree().quit()
