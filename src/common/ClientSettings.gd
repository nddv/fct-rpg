#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

const CONF_FILE: String = "user://client/settings.cfg"

var config: ConfigFile = ConfigFile.new()

func _ready() -> void:
	if !OS.has_feature("dedicated_server"):
		if config.load(CONF_FILE) == ERR_FILE_NOT_FOUND:
			var dir: Directory = Directory.new()
			dir.make_dir_recursive("user://client")
			config.set_value("general", "locale", TranslationServer.get_locale())
		else:
			load_settings()

func load_settings() -> void:
	TranslationServer.set_locale(config.get_value("general", "locale"))

func store(section: String, key: String, value) -> void:
	config.set_value(section, key, value)
	config.save(CONF_FILE)
