#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Area2D

class_name Player

#var username

onready var ray: RayCast2D = $"RayCast2D"

const INPUT_DIRS: Dictionary = {"ui_up": Vector2.UP,
								"ui_right": Vector2.RIGHT,
								"ui_down": Vector2.DOWN,
								"ui_left": Vector2.LEFT}
var last_move_time: int

func update_move_time() -> bool:
	if OS.get_ticks_msec() - last_move_time > Globals.MOVE_TIME:
		last_move_time = OS.get_ticks_msec()
		return true
	else:
		return false

func get_cell_coordinates() -> Vector2:
	return Vector2(int(position.x)/Globals.TILE_SIZE, int(position.y)/Globals.TILE_SIZE)
