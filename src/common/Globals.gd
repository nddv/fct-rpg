#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

const TILE_SIZE: int = 16

const MOVE_TIME: int = 500 # msecs

const MOVE_TIME_SECONDS: float = MOVE_TIME / 1000.0

const MAX_MESSAGE_LENGTH: int = 500 # chars

enum SERVER_RESPONSE {
	UNKNOWN_COMMAND,
	MISSING_PRIVILIGE,
	PRIV_GRANTED,
	PRIV_REVOKED,
	PLAYER_KICKED
}


const WORLD_OPTIONS: Array = ["port", "max-players", "admin-user"]

const POSSIBLE_PRIVS: Array = ["shout", "privs", "ban"]

var server_options: Dictionary


var in_game: bool = false

var lsd_mode: bool = false
