#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This autoload is used for signals that are emitted accross multiple places in the project
# Connect to it using the connect method and emit a single shared signal with emit_signal.

extends Node

# Client with id id needs a full resync.
signal full_sync(id)

signal change_menu(menu)

signal leave_game()

signal player_joined(player_id, player_username)

signal player_left(player_id, player_username)

signal lsd_moving(time)

signal lsd_moving_reset(time)
