#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

# WorldRWS is responsible for the loading and saving of world data, and the retrieving of it.

enum MISSION_TYPE {
	ACTIVE,
	COMPLETED
}

const SQLite: Resource = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")

var world_meta: ConfigFile
var player_db: SQLite

func open_world(path: String, world_name: String = "") -> bool:
	if File.new().file_exists(path + "/world.cfg"):
		load_world_meta(path + "/world.cfg")
		load_player_db(path + "/players.sqlite")
		return false
	else:
		create_world(path, world_name)
		return true

func load_world_meta(path: String) -> void:
	world_meta = ConfigFile.new()
	world_meta.load(path)

func load_player_db(path: String) -> void:
	player_db = SQLite.new()
	player_db.path = path
	player_db.open_db()

func create_world(path: String, world_name: String) -> void:
	var dir: Directory = Directory.new()
	dir.make_dir_recursive(path)

	world_meta = ConfigFile.new()

	var accounts_dict: Dictionary
	var player_pos: Dictionary
	var privs_dict: Dictionary
	var missions_dict: Dictionary
	accounts_dict["uid"] = {
		"data_type" : "int",
		"primary_key" : true,
		"auto_increment" : true
	}
	accounts_dict["username"] = {
		"data_type" : "text",
		"not_null" : true,
		"unique" : true # ToDo: Ignored for some reason.
	}
	accounts_dict["p_hash"] = {
		"data_type" : "blob",
		"not_null" : true
	}
	accounts_dict["p_salt"] = {
		"data_type" : "blob",
		"not_null" : true
	}

	player_pos["uid"] = {
		"data_type" : "int",
		"primary_key" : true,
		"auto_increment" : true
	}
	player_pos["map"] = {
		"data_type" : "int"
	}
	player_pos["pos_x"] = {
		"data_type" : "int"
	}
	player_pos["pos_y"] = {
		"data_type" : "int"
	}

	privs_dict["uid"] = {
		"data_type" : "int",
		"foreign_key" : true,
		"not_null" : true
	}
	privs_dict["privilige"] = {
		"data_type" : "text",
		"not_null" : true
	}

	# unique (uid, mission_name)? Nah. (uid, mission_type, mission_name) due to changing mission indexes.
	missions_dict["uid"] = {
		"data_type" : "int",
		"foreign_key" : true,
		"not_null" : true
	}
	missions_dict["mission_type"] = {
		"data_type" : "int",
		"not_null" : true
	}
	missions_dict["mission_name"] = {
		"data_type" : "text",
		"not_null" : true
	}
	missions_dict["mission_index"] = {
		"data_type" : "int",
		"not_null" : true
	}

	player_db = SQLite.new()
	player_db.path = path + "/players.sqlite"
	if not OS.has_feature("standalone"):
		player_db.verbose_mode = true
	player_db.open_db()
	player_db.create_table("accounts", accounts_dict)
	player_db.create_table("player_pos", player_pos)
	player_db.create_table("priviliges", privs_dict)
	player_db.create_table("missions", missions_dict)
	player_db.query("CREATE UNIQUE INDEX priv_uniq_index ON priviliges(uid, privilige);") # ToDo: I don't know anything about databases, except the basics.
	player_db.query("CREATE UNIQUE INDEX mis_uniq_index ON missions(uid, mission_name);")
	player_db.query("CREATE UNIQUE INDEX mis_indx_uniq_index ON missions(uid, mission_type, mission_index)")

func update_world_config(world_path: String, options: Dictionary) -> void:
	for option in Globals.WORLD_OPTIONS:
		world_meta.set_value("general", option, options[option])

	world_meta.save(world_path + "/world.cfg")


# Registers a new player.
#
# A new entry for a player is added to the player database, with the given username and password (stored salted and hashed)
# Returns true if succesful, false otherwise.
func register_player(username: String, password: String) -> bool:
	# ToDo: Use better hashing algorithm.
	var crypto: Crypto = Crypto.new()
	var hc: HashingContext = HashingContext.new()
	var p_salt: PoolByteArray = crypto.generate_random_bytes(64)
	var p_hash: PoolByteArray = (password + p_salt.get_string_from_ascii()).sha256_buffer()
	var entry: Dictionary = {
		"username" : username,
		"p_hash" : p_hash,
		"p_salt" : p_salt
	}
	var success: bool = player_db.insert_row("accounts", entry)
	if success:
		var uid: int = get_player_uid(username)
		var pos_entry: Dictionary = {
			"uid" : uid
		}
		var priv_entry: Dictionary = {
			"uid" : uid,
			"privilige" : "shout"
		}
		player_db.insert_row("player_pos", pos_entry)
		player_db.insert_row("priviliges", priv_entry)
	return success

# Checks if user can authenticate with credentials given.
#
# Returns true if user exists and if password is correct, returns false otherwise.
func verify_login(username: String, password: String) -> bool:
	player_db.query_with_bindings("SELECT p_hash, p_salt FROM accounts WHERE username = ?", [username])
	var result: Array = player_db.query_result
	if result.empty():
		return false
	return (password + result[0]["p_salt"].get_string_from_ascii()).sha256_buffer() == result[0]["p_hash"]

func save_player_pos(username: String, map: int, pos: Vector2) -> void:
	var uid: int = get_player_uid(username)
	var pos_row: Dictionary = {
		"uid": uid,
		"map": map,
		"pos_x": pos.x,
		"pos_y": pos.y
	}
	player_db.update_rows("player_pos", "uid = " + str(uid), pos_row)

func get_player_pos(username: String):
	var uid = get_player_uid(username)
	player_db.query_with_bindings("SELECT map, pos_x, pos_y FROM player_pos WHERE uid = ?;", [uid])
	var result: Array = player_db.query_result
	if result[0]["map"] == null:
		return null
	else:
		return [result[0]["map"], Vector2(result[0]["pos_x"], result[0]["pos_y"])]


func has_privilige(username: String, priv: String) -> bool:
	player_db.query_with_bindings("SELECT privilige FROM priviliges WHERE uid = ? AND privilige = ? LIMIT 1;", [get_player_uid(username), priv])
	return !player_db.query_result.empty()

func add_privilige(username: String, priv: String) -> void:
	if priv in Globals.POSSIBLE_PRIVS:
		player_db.insert_row("priviliges", {"uid": get_player_uid(username), "privilige": priv})

func remove_privilige(username: String, priv: String) -> void:
	var uid: int = get_player_uid(username)
	if priv in Globals.POSSIBLE_PRIVS:
		player_db.delete_rows("priviliges", "uid = %d AND privilige = \"%s\"" % [uid, priv])


func get_missions_by_type(username: String, type: int) -> Array:
	var query_result: Array = player_db.select_rows("missions", "uid = " + str(get_player_uid(username)) + " AND mission_type = " + str(type), ["mission_name", "mission_index"])
	var sorted_query_result = query_result
	sorted_query_result.sort_custom(self, "index_sort")
	var result: Array = []
	for row in sorted_query_result:
		result.append(row["mission_name"])
	return result
# Only 4.0 has lambda functions. :/
func index_sort(row1, row2): return row1["mission_index"] < row2["mission_index"]

func assign_mission(username: String, type: int, mis_name: String) -> void:
	player_db.query("SELECT max(mission_index) FROM missions")
#	print(player_db.query_result)
	var mis_index: int = player_db.query_result[0]["max(mission_index)"] + 1 if player_db.query_result[0]["max(mission_index)"] else 0
	player_db.insert_row("missions", {"uid" : get_player_uid(username), "mission_type": type, "mission_name": mis_name, "mission_index": mis_index})

func change_mission_status(username: String, mis_name, new_type: int) -> void:
	player_db.query("SELECT max(mission_index) FROM missions")
	var new_index: int = player_db.query_result[0]["max(mission_index)"] + 1
	# Nothing using string concatenation to build the query here, as all the inputs come from the server, injections therefore not being a problem.
	player_db.update_rows("missions", "uid = " + str(get_player_uid(username)) + " AND mission_name = \"" + mis_name + "\"", {"mission_type": new_type, "mission_index": new_index})

func get_player_uid(username: String) -> int:
	player_db.query_with_bindings("SELECT uid FROM accounts WHERE username = ?;", [username])
	return player_db.query_result[0]["uid"]
