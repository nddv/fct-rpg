#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

var active_missions: Array = []
# Order here matters. It defines in what order the permanent success effects are applied.
var completed_missions: Array = []
onready var player: Player = get_parent()

func _ready() -> void:
	if Network.is_server():
		for mis_name in WorldRWS.get_missions_by_type(player.username, WorldRWS.MISSION_TYPE.ACTIVE):
			assign_mission(mis_name, false, false)
		for mis_name in WorldRWS.get_missions_by_type(player.username, WorldRWS.MISSION_TYPE.COMPLETED):
			assign_mission(mis_name, false, false)
			update_mission_status(mis_name, true, false, false)

### COMMON

# ToDo: Potential dangerous code execution.
func assign_mission(mis_name: String, check_assignment_requirements: bool = true, session_assignment_effects: bool = true, offline: bool = false) -> bool:
	# Prevent duplicate entries
	for m in active_missions + completed_missions:
		if m.mission_name == mis_name:
			return false

	var mis: Mission = load("res://src/common/missions/" + mis_name + ".gd").new()
	mis.name = mis_name
	add_child(mis)
	if not check_assignment_requirements or mis.meets_assignment_requirements(player):
		if session_assignment_effects:
			mis.assignment_effects_common(player)
		if Network.is_server():
			if session_assignment_effects:
				mis.assignment_effects_server(player)
		else:
			if not offline:
				rpc_id(1, "mission_assignment_request", mis_name)
			if session_assignment_effects:
				mis.assignment_effects_client(player)
			mis.assignment_effects_permanent(player)
		active_missions.append(mis)
		return true
	else:
		remove_child(mis)
		return false

func update_mission_status(mis_name: String, force: bool = false, check_success_requirements: bool = true, session_success_effects: bool = true, offline: bool = false) -> bool:
	for i in len(active_missions):
		if active_missions[i].mission_name == mis_name and (not check_success_requirements or ((force or !active_missions[i].manual_update) and active_missions[i].meets_success_requirements(player))):
			var completed_mission: Mission = active_missions.pop_at(i)
			completed_missions.push_back(completed_mission)
			if session_success_effects:
				completed_mission.success_effects_common(player)
			if Network.is_server():
				if session_success_effects:
					completed_mission.success_effects_server(player)
			else:
				if not offline:
					rpc_id(1, "update_mission_status_request", mis_name)
				if session_success_effects:
					completed_mission.success_effects_client(player)
				completed_mission.success_effects_permanent(player)
			return true
	return false

### SERVER

func update_all_mission_status(force: bool = false) -> void:
	for mis in active_missions:
		if (force or !mis.manual_update) and mis.meets_success_requirements(player):
			var completed_mission: Mission = active_missions.pop_back()
			completed_missions.push_back(completed_mission)
			mis.success_effects_server(player)
			rpc_id(player.get_network_master(), "force_mission_assignment_request", mis.mission_name)

remote func mission_assignment_request(mis_name: String):
	var id: int = get_tree().get_rpc_sender_id()
	if not assign_mission(mis_name, true):
		Events.emit_signal("full_sync", id)
	else:
		WorldRWS.assign_mission(player.username, WorldRWS.MISSION_TYPE.ACTIVE, mis_name)

remote func update_mission_status_request(mis_name: String):
	var id: int = get_tree().get_rpc_sender_id()
	if not update_mission_status(mis_name, true):
		Events.emit_signal("full_sync", id)
	else:
		WorldRWS.change_mission_status(player.username, mis_name, WorldRWS.MISSION_TYPE.COMPLETED)

### CLIENT

remote func force_mission_assignment_request(mis_name: String):
	assign_mission(mis_name, false)

remote func force_update_mission_status_request(mis_name: String):
	update_mission_status(mis_name, true, false)
