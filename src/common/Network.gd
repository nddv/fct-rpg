#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

signal register_response(success)
signal auth_response(success)

var authenticated_players: Dictionary # ToDo: Unnecessary to be a dictionary. We only need the id

var username: String
var password: String
var register: bool

func _ready() -> void:
	get_tree().connect("network_peer_connected", self, "_network_peer_connected")
	get_tree().connect("network_peer_disconnected", self, "_network_peer_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	get_tree().connect("connection_failed", self, "_connection_failed")
	get_tree().connect("server_disconnected", self, "_server_disconnected")



func _network_peer_connected(id: int) -> void:
	print("Peer ", id, " connected.")

func _network_peer_disconnected(id: int) -> void:
	if id in authenticated_players:
		Events.emit_signal("player_left", id, authenticated_players[id])
	print("Peer ", id, " disconnected.")

func _connected_to_server() -> void:
	print("Connected to server.")
	if !register:
		rpc_id(1, "auth_user", username, password)
	else:
		rpc_id(1, "register_user", username, password)

func _connection_failed() -> void:
	print("Connection failed.")

func _server_disconnected() -> void:
	Events.emit_signal("leave_game")
	print("Disconnected from server.")



func is_server() -> bool:
	return get_tree().get_network_unique_id() == 1



func server_set_up(port: int, max_players: int) -> int:
	var peer: NetworkedMultiplayerENet = NetworkedMultiplayerENet.new()
	var err: int = peer.create_server(port, max_players)
	get_tree().network_peer = peer
	return err

remote func auth_user(username: String, password: String) -> void:
	var id: int = get_tree().get_rpc_sender_id()

	if username in authenticated_players.values():
		rpc_id(id, "kick")
		return

	var authenticated: bool = WorldRWS.verify_login(username, password)
	rpc_id(id, "auth_response", authenticated)

	if authenticated:
		authenticated_players[id] = username
		print(id, " authenticated.")
		Events.emit_signal("player_joined", id, username)
	else:
		get_tree().network_peer.disconnect_peer(id)
		print(id, " authentication failed. Disconnected.")

remote func register_user(username: String, password: String) -> void:
	var id: int = get_tree().get_rpc_sender_id()
	var ret: bool = WorldRWS.register_player(username, password)
	rpc_id(id, "register_response", ret)

# Find other solution if we use this more
func get_username_id(username: String) -> int:
	var index: int = authenticated_players.values().find(username)
	return authenticated_players.keys()[index] if index != -1 else ""



func client_set_up(ip: String, port: int, username: String, password: String, register: bool = false) -> int:
	self.username = username
	self.password = password
	self.register = register
	var peer: NetworkedMultiplayerENet = NetworkedMultiplayerENet.new()
	var err: int = peer.create_client(ip, port)
	get_tree().network_peer = peer
	return err

remote func auth_response(authenticated: bool) -> void:
	emit_signal("auth_response", authenticated)

func register_request(username: String, password: String) -> void:
	rpc_id(1, "register_user", username, password)

remote func register_response(success: bool) -> void:
	emit_signal("register_response", success)

remote func kick() -> void:
	Events.emit_signal("leave_game")
