#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Mission

const mission_name = "MariaHug"
const manual_update: bool = true

var maria: NPC
var miguel: NPC

func _ready():
	maria = $"/root/Main/m0/Maria"
	miguel = $"/root/Main/m0/Miguel"

func meets_assignment_requirements(player: Player) -> bool:
	return player.global_position.distance_squared_to(maria.global_position) <= Globals.TILE_SIZE * Globals.TILE_SIZE

func meets_success_requirements(player: Player) -> bool:
	return player.global_position.distance_squared_to(miguel.global_position) <= Globals.TILE_SIZE * Globals.TILE_SIZE

func assignment_effects_common(player: Player) -> void:
	return

func assignment_effects_server(player: Player) -> void:
	return

func assignment_effects_client(player: Player) -> void:
	return

func assignment_effects_permanent(player: Player) -> void:
	maria.client_next_dialogue_title = "maria_general"
	miguel.client_next_dialogue_title = "miguel_mariahug_mission"

func success_effects_common(player: Player) -> void:
	return

func success_effects_server(player: Player) -> void:
	return

func success_effects_client(player: Player) -> void:
	return

func success_effects_permanent(player: Player) -> void:
	miguel.client_sprite_texture = preload("res://src/client/assets/gfx/person-happy.png")
	miguel.client_next_dialogue_title = "miguel_general_happy"
