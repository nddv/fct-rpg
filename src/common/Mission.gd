#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

class_name Mission

#const mission_name: String # Needs to be defined in inheriting classes and must be equal to filename without the extension
#const manual_update: bool # Needs to be defined in inheriting classes

func meets_assignment_requirements(player: Player) -> bool:
	assert(false, "method not implemented")
	return false

func meets_success_requirements(player: Player) -> bool:
	assert(false, "method not implemented")
	return false

func assignment_effects_common(player: Player) -> void:
	assert(false, "method not implemented")

func assignment_effects_server(player: Player) -> void:
	assert(false, "method not implemented")

func assignment_effects_client(player: Player) -> void:
	assert(false, "method not implemented")

func assignment_effects_permanent(player: Player) -> void:
	assert(false, "method not implemented")

func success_effects_common(player: Player) -> void:
	assert(false, "method not implemented")

func success_effects_server(player: Player) -> void:
	assert(false, "method not implemented")

func success_effects_client(player: Player) -> void:
	assert(false, "method not implemented")

# Permanent success effects on client (gets called at the beginning of every session, if completed previously).
func success_effects_permanent(player: Player) -> void:
	assert(false, "method not implemented")
