#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends MarginContainer

var errors: Array

func _ready() -> void:
	Network.connect("register_response", self, "_on_register_response")
	Network.connect("auth_response", self, "_on_auth_response")

	if not OS.has_feature("standalone"): # To speed up debug runs.
		$VBoxContainer/GridContainer/Host.text = "localhost"
		$VBoxContainer/GridContainer/Port.text = "4242"

func is_valid_port(port: String) -> bool:
	return port.is_valid_integer() and int(port) > 0 and int(port) <= 65535

func add_error(error: String) -> void:
	if !(error in errors):
		errors.append(error)
		update_error_list()

func remove_error(error: String) -> void:
	if error in errors:
		errors.erase(error)
		update_error_list()

func clear_errors() -> void:
	errors.clear()
	update_error_list()

func update_error_list() -> void:
	var result: String = ""
	for err in errors:
		result += err + "\n"
	result = result.trim_suffix("\n")
	$"VBoxContainer/Control/Errors".text = result

func connect_to_server(register: bool) -> void:
	var host: String = $"VBoxContainer/GridContainer/Host".text
	var port: String = $"VBoxContainer/GridContainer/Port".text
	var username: String = $"VBoxContainer/GridContainer/Username".text
	var password: String = $"VBoxContainer/GridContainer/Password".text
	if is_valid_port(port):
		clear_errors()
		$"VBoxContainer/GridContainer/Password".clear()
		Network.client_set_up(host, int(port), username, password, register)


func _on_register_response(success: bool) -> void:
	if success:
		add_error(tr("CONNECT_REGISTER_SUCCESS"))
	else:
		add_error(tr("CONNECT_REGISTER_FAILED"))
	get_tree().network_peer = null

func _on_auth_response(success: bool) -> void:
	if success:
		add_error(tr("CONNECT_AUTH_SUCCESS"))
	else:
		add_error(tr("CONNECT_AUTH_FAILED"))

func _on_Cancel_pressed() -> void:
#	get_tree().change_scene("res://src/client/menu/MainMenu.tscn")
	Events.emit_signal("change_menu", "MainMenu")

func _on_Register_pressed() -> void:
	connect_to_server(true)

func _on_Port_text_changed(new_text: String) -> void:
	var invalid_port: String = tr("CONNECT_INVALID_PORT")
	if !is_valid_port(new_text):
		add_error(invalid_port)
	else:
		remove_error(invalid_port)

func _on_Join_pressed(lineContent = "") -> void:
	connect_to_server(false)
