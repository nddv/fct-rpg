#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends MarginContainer

onready var language = $"VBoxContainer/GridContainer/Language"

func _ready():
	var locales: Array = TranslationServer.get_loaded_locales()
	for locale in locales:
		language.add_item(locale)
	language.select(locales.find(TranslationServer.get_locale()))

func _on_Save_pressed() -> void:
	var locale = language.get_item_text(language.selected)
	TranslationServer.set_locale(locale)
	ClientSettings.store("general", "locale", locale)

func _on_Cancel_pressed() -> void:
	Events.emit_signal("change_menu", "MainMenu")
