#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends CanvasLayer

signal push_message(message)

onready var message_field: LineEdit = $PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Message
onready var chat_log: Label = $PanelContainer/MarginContainer/VBoxContainer/Control/Log
onready var log_container: Control = $PanelContainer/MarginContainer/VBoxContainer/Control
onready var scroll_step: int = log_container.rect_size.y / 2
var old_message_log: Array = [""]
var old_index: int = 0

func _ready() -> void:
	chat_log.set_message_translation(false)
	chat_log.notification(NOTIFICATION_TRANSLATION_CHANGED)

func _input(event: InputEvent) -> void:
	if event.is_action("chat") and Globals.in_game and not visible:
		Events.emit_signal("change_menu", "Chat")
		message_field.grab_focus()
		message_field.call_deferred("clear") # quick workaround, to avoid having the chat letter appear in the LineEdit
	elif event.is_action("ui_cancel") and visible:
		Events.emit_signal("change_menu", "GameHUD")
	elif event.is_action_pressed("ui_page_up") and visible:
		chat_log.rect_global_position.y += scroll_step
		if chat_log.rect_global_position.y > log_container.rect_global_position.y:
			chat_log.rect_global_position.y = log_container.rect_global_position.y
	elif event.is_action_pressed("ui_page_down") and visible:
		chat_log.rect_position.y -= scroll_step
		if log_container.rect_global_position.y + log_container.rect_size.y > chat_log.rect_global_position.y + chat_log.rect_size.y:
			chat_log.rect_global_position.y = log_container.rect_global_position.y + log_container.rect_size.y - chat_log.rect_size.y
	elif event.is_action_pressed("ui_up") and visible:
		if old_index > 0:
			old_message_log[old_index] = message_field.text
			old_index -= 1
			message_field.text = old_message_log[old_index]
	elif event.is_action_pressed("ui_down") and visible:
		if old_index + 1 < old_message_log.size():
			old_message_log[old_index] = message_field.text
			old_index += 1
			message_field.text = old_message_log[old_index]

remote func receive_message(username: String, message: String) -> void:
	# ToDo: In the future use timestamps
	var chat_message: String = "\n" + username + ": " + message
	chat_log.text += chat_message
	emit_signal("push_message", chat_message)

remote func server_message(type: int, key_val: Dictionary) -> void:
	var chat_message: String = "\n" + tr("CHAT_SERVER").to_upper() + ": "

	match type:
		Globals.SERVER_RESPONSE.UNKNOWN_COMMAND:
			chat_message += tr("CHAT_UNKNOW_COMMAND")
		Globals.SERVER_RESPONSE.MISSING_PRIVILIGE:
			chat_message += tr("CHAT_INSUFFICIENT_PRIVILIGE")
		Globals.SERVER_RESPONSE.PRIV_GRANTED:
			# keys: granter, priv, grantee
			chat_message += tr("CHAT_PRIVILIGE_GRANTED").format(key_val)
		Globals.SERVER_RESPONSE.PRIV_REVOKED:
			# keys: revoker, priv, revokee
			chat_message += tr("CHAT_PRIVILIGE_REVOKED").format(key_val)
		Globals.SERVER_RESPONSE.PLAYER_KICKED:
			# keys: kicker, kicked
			chat_message += tr("CHAT_PLAYER_KICKED").format(key_val)
	chat_log.text += chat_message
	emit_signal("push_message", chat_message)

remote func me(username: String, message: String) -> void:
	var chat_message: String = "\n*" + username + " " + message + "*"
	chat_log.text += chat_message
	emit_signal("push_message", chat_message)

remote func player_joined(username: String) -> void:
	var chat_message: String = "\n" + tr("CHAT_PLAYER_JOINED").format({"player": username})
	chat_log.text += chat_message
	emit_signal("push_message", chat_message)

remote func player_left(username: String) -> void:
	var chat_message: String = "\n" + tr("CHAT_PLAYER_LEFT").format({"player": username})
	chat_log.text += chat_message
	emit_signal("push_message", chat_message)

func _on_Send_pressed(entered_text = "") -> void:
	var message: String = message_field.text
	message_field.clear()

	if message.length() > 0 :
		old_message_log[old_index] = message
		old_index += 1
		old_message_log.append("")
		if message.length() < Globals.MAX_MESSAGE_LENGTH:
			rpc_id(1, "send_message", message)
