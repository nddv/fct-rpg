#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node

var currently_shown_menu: String = "MainMenu"
var lsd_moving_time_elapsed: float

onready var screen_shader: ColorRect = $ScreenShader/ColorRect

var time: float = 0

func _ready():
	Network.connect("auth_response", self, "_on_auth_response")
	Events.connect("change_menu", self, "change_menu")
	Events.connect("leave_game", self, "leave_game")
	Events.connect("lsd_moving", self, "lsd_moving")
	Events.connect("lsd_moving_reset", self, "lsd_moving_reset")
	$Chat.connect("push_message", $GameHUD/MarginContainer/Control/ChatPreview, "push_message")

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("lsd"):
		Globals.lsd_mode = !Globals.lsd_mode
		$ScreenShader.visible = !$ScreenShader.visible
		AudioServer.set_bus_bypass_effects(1, !AudioServer.is_bus_bypassing_effects(1))

func _process(delta: float) -> void:
	time += delta
	AudioServer.get_bus_effect(1, 0).pitch_scale = sin(time*0.2)/4 + 0.3
	AudioServer.get_bus_effect(1, 1).pan = sin(time*0.4)

func change_menu(menu: String) -> void:
	# empty string means "do not display any menu"
	if currently_shown_menu != "":
		get_node(currently_shown_menu).hide()
	if menu != "":
		get_node(menu).show()
	currently_shown_menu = menu

func _on_auth_response(success: bool) -> void:
	if success:
		var map: TileMap = preload("res://src/client/maps/test-tilemap.tscn").instance()
		map.name = "m" + str(0) # ToDo: Multiple map ids
		map.add_to_group("map")
		add_child(map)
		Events.emit_signal("change_menu", "GameHUD")
		Globals.in_game = true
		$AudioStreamPlayer.play()

remote func full_sync(game_state: Array) -> void: # ToDo: Rewrite
	var player_scene: PackedScene = preload("res://src/client/Player.tscn") #TODO: correct type?
	for player in get_tree().get_nodes_in_group("player"):
		player.queue_free()
	for map in range(len(game_state)): #TODO: Disallow clients with the same username.
		var map_node = get_node("m" + str(map))
		for player in game_state[map]["player"].keys():
			var new_player: PlayerC = player_scene.instance()
			var story = new_player.get_node("Story")
			new_player.name = "p" + str(player)
			new_player.set_network_master(player)
			map_node.add_child(new_player)
			new_player.username = game_state[map]["player"][player]["username"]
			new_player.position = game_state[map]["player"][player]["position"]
			if new_player.is_network_master():
				for mis_name in game_state[map]["player"][player]["active_missions"]:
						story.assign_mission(mis_name, false, false, true)
				for mis_name in game_state[map]["player"][player]["completed_missions"]:
					story.assign_mission(mis_name, false, false, true)
					story.update_mission_status(mis_name, true, false, false, true)
		for npc_name in game_state[map]["npc"].keys():
			var npc: NPCC = map_node.get_node(npc_name)
			npc.position = game_state[map]["npc"][npc_name]["position"]

remote func add_player(map: int, id: int, username: String, position: Vector2) -> void:
	if id != get_tree().get_network_unique_id(): # The new client should get a full_sync, not an add_player
		var new_player: PlayerC = preload("res://src/client/Player.tscn").instance()
		new_player.name = "p" + str(id)
		new_player.username = username
		new_player.position = position
		new_player.set_network_master(id)
		get_node("m" + str(map)).add_child(new_player)

remote func remove_player(id: int) -> void:
	for player in get_tree().get_nodes_in_group("player"):
		if player.name == "p" + str(id):
			player.get_parent().remove_child(player)
			player.queue_free()
			return

func leave_game() -> void:
	$AudioStreamPlayer.stop()
	Globals.in_game = false
	get_tree().set_deferred("network_peer", null)
	Events.emit_signal("change_menu", "ConnectMenu")
	for map in get_tree().get_nodes_in_group("map"):
		map.queue_free()

# Needed to avoid weird doppler effect when moving vertically in LSD mode.
func lsd_moving(time_elapsed: float):
	screen_shader.get_material().set_shader_param("time_moving_passed", screen_shader.get_material().get_shader_param("time_moving_passed") + (time_elapsed - lsd_moving_time_elapsed))
	lsd_moving_time_elapsed = time_elapsed

func lsd_moving_reset():
	lsd_moving_time_elapsed = 0
