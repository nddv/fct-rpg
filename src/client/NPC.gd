#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends NPC

class_name NPCC

export(Texture) var client_sprite_texture setget set_sprite_texture
export(Resource) var client_dialogue_resource
export(String) var client_next_dialogue_title

var actionable = true

onready var label = $Sprite/Node2D/Label
onready var sprite = $Sprite
onready var tween = $Sprite/Tween

func _ready() -> void:
	label.text = common_name_NPC
	label.rect_position = Vector2(-Globals.TILE_SIZE, -Globals.TILE_SIZE)
	label.rect_size = Vector2(Globals.TILE_SIZE * 2, Globals.TILE_SIZE / 2)
	label.set_message_translation(false)
	label.notification(NOTIFICATION_TRANSLATION_CHANGED)
	set_name(common_name_NPC)
	set_sprite_texture(client_sprite_texture)

func set_sprite_texture(t: Texture) -> void:
	client_sprite_texture = t # ToDo: Does Godot already do that?
	if is_inside_tree():
		sprite.texture = t

func show_dialogue_balloon(title: String, local_resource: DialogueResource = null) -> void:
	var dialogue = yield(DialogueManager.get_next_dialogue_line(title, local_resource), "completed")
	if dialogue != null:
		var balloon = preload("res://src/client/DialogueBalloon.tscn").instance()
		balloon.dialogue = dialogue
		get_tree().current_scene.add_child(balloon)
		show_dialogue_balloon(yield(balloon, "actioned"), local_resource)
	else:
		Events.emit_signal("change_menu", "GameHUD")
		actionable = true
		rpc_id(1, "unactioned")

func action(player: PlayerC) -> void:
	if actionable:
		actionable = false
		Events.emit_signal("change_menu", "")
		var dialogue: PackedScene = preload("res://src/client/DialogueBalloon.tscn")
		show_dialogue_balloon(client_next_dialogue_title, client_dialogue_resource)
		$AudioStreamPlayer.play()
		rpc_id(1, "actioned")

remote func change_pos(new_pos: Vector2):
	tween.interpolate_property(sprite, "global_position", position, new_pos, Globals.MOVE_TIME_SECONDS, Tween.TRANS_LINEAR)
	tween.start()
	position = new_pos
