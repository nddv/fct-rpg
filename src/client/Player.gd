#     FCT RPG
#     Copyright (C) 2021  Núcleo de Design e Desenvolvimento de Videojogos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Player

class_name PlayerC

var username: String setget set_username

var enable_moving: bool = true
var own_player: bool

onready var tween: Tween = $"Sprite/Tween"
onready var sprite: Sprite = $Sprite

func set_username(value: String) -> void:
	username = value
	$"Sprite/Node2D/Label".text = username

func _ready() -> void:
	Events.connect("change_menu", self, "_on_change_menu")
	var label = $Sprite/Node2D/Label
	label.rect_position = Vector2(-Globals.TILE_SIZE, -Globals.TILE_SIZE)
	label.rect_size = Vector2(Globals.TILE_SIZE * 2, Globals.TILE_SIZE / 2)

	label.set_message_translation(false)
	label.notification(NOTIFICATION_TRANSLATION_CHANGED)

	if is_network_master():
		own_player = true

		var camera = Camera2D.new()
		camera.current = true
		sprite.add_child(camera)

		DialogueManager.game_states.append($Story)

func _input(event) -> void:
	if event.is_action_pressed("action"):
		ray.force_raycast_update()
		var object = ray.get_collider()
		if ray.is_colliding() and object.has_method("action"):
			object.action(self)
	if is_network_master() and !tween.is_active() and enable_moving: # ToDo: Failed moves shouldn't update move time
		var move_action: String = get_move_action(event)
		# ToDo: I hope GDScript doesn't run the second part of an already false statement
		if move_action != "" and update_move_time():
			move(move_action)

func move(input_dir: String) -> void:
	var old_position: Vector2 = global_position
	var new_position: Vector2 = global_position + INPUT_DIRS[input_dir] * Globals.TILE_SIZE
	ray.cast_to = INPUT_DIRS[input_dir] * Globals.TILE_SIZE
	ray.collide_with_areas = true # to allow to collisions with players and other moving objects
	for body in get_overlapping_areas():
		ray.add_exception(body)

	ray.force_raycast_update()
	if !ray.is_colliding():
		position = new_position
		sprite.global_position = old_position
		tween.interpolate_property(sprite, "global_position", old_position, new_position, Globals.MOVE_TIME_SECONDS, Tween.TRANS_LINEAR)
		tween.start()
		rpc_id(1, "movement_input", input_dir)
	ray.clear_exceptions()
	ray.add_exception(self)

remote func change_pos(new_pos: Vector2) -> void:
	if (!is_network_master() and get_tree().get_rpc_sender_id() == 1): # Only the server should call this.
		tween.interpolate_property(sprite, "global_position", position, new_pos, Globals.MOVE_TIME_SECONDS, Tween.TRANS_LINEAR)
		tween.start()
		position = new_pos

func get_move_action(event: InputEvent = null) -> String:
	if event == null:
		for action in INPUT_DIRS:
			if Input.is_action_pressed(action):
				return action
	else:
		for action in INPUT_DIRS:
			if event.is_action_pressed(action):
				return action
	return ""

func _on_change_menu(name: String) -> void:
	if  name == "GameHUD":
		enable_moving = true
	else:
		enable_moving = false

func _on_Tween_tween_completed(object: Object, key: NodePath) -> void:
	Events.emit_signal("lsd_moving_reset")
	var action: String = get_move_action()
	if action != "":
		move(action)

func _on_Player_tree_exiting() -> void:
	if own_player:
		DialogueManager.game_states.erase($Story)

func _on_Tween_tween_step(object: Object, key: NodePath, elapsed: float, value: Object) -> void:
	if own_player:
		if $RayCast2D.cast_to.y < 0:
			Events.emit_signal("lsd_moving", elapsed)
