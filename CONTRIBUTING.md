# Contributing

## Getting started

- Download the standard version of [Godot](https://godotengine.org/) 3.5. 
- Download and install [Git](https://git-scm.com/).
- Clone the repository with `git clone https://gitlab.com/nddv/fct-rpg.git`.
- `cd` into the newly created directory and set your name and email via `git config --local user.name "Maria Albertina" && git config --local user.email "m.albertina@example.com"`.
- Open Godot, press import and navigate to the newly created directory and select project.godot and import it.

You now have everything needed to make some basic contributions. Just make your changes, `git add -A`, `git commit` and then `git push origin branch-you-want-to-push-to`. You should definetly read up on how to use Godot and Git, though, if you haven't done so already.

## Going further

To learn how to use Godot see: [https://docs.godotengine.org/en/stable/getting_started/step_by_step/index.html](https://docs.godotengine.org/en/stable/getting_started/step_by_step/index.html)

To learn how to use Git see:

- [https://rogerdudler.github.io/git-guide/](https://rogerdudler.github.io/git-guide/)
- [https://learngitbranching.js.org/](https://learngitbranching.js.org/)
- [https://www.git-scm.com/book/en/v2/](https://www.git-scm.com/book/en/v2/)

You might want to use your own prefered tools (e.g. your favourite IDE) to work on this project. In that case you must do your own research to see how to do so. For example, if you want to use VS Code, [this](https://github.com/godotengine/godot-vscode-plugin) may be useful, and if you want to use Emacs, [this](https://github.com/godotengine/emacs-gdscript-mode) may be useful. There are too many tools in this world, so we can't help with all of them.

## Art
Currently this game is using "programmer's art" for its assets. What is currently there is not set in stone and can (and probably should) be changed. Even the tile size (currently 16x16 pixels) is up for debate. You should try to coordinate your work with other fellow artists over on the Discord.
We have need for textures and sound.

## Translations
As of the the time this is being written<!--grammar?-->, there are three translations (albeit only one of them is complete). European Portuguese, English and German. More translations can be added.
We use gettext for these translations. To extract strings from the code and dialogues and generate the POTs and PO files, you should use the utils/update_localization.py script. You may then use a tool like POedit to edit the translations.

## Mapping
All of the game's maps can be found in src/client/maps. You can instance and edit objects over there or draw new cells or change existing ones.
After you are done and want to test your changes you must use the utils/map_conversion.py script to generate the map files for the server. If you don't, weird stuff will happen when testing the game.

